﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputKeyboard : IInput
{
    Vector3 dir = Vector3.zero;

    public Vector3 getAxis()
    {
        dir = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        return dir.normalized;
    }

    public bool isPressing()
    {
        return Input.anyKey;
    }

    public bool getFire()
    {
        return Input.GetKeyDown(KeyCode.Mouse0);
    }

    public void Update(){}

    public Vector3 getFireAxis()
    {
        throw new NotImplementedException();
    }
}
