﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInput{

    Vector3 getAxis();
    Vector3 getFireAxis();
    bool getFire();
    void Update();
    bool isPressing();
}
