﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputMobile : IInput {

    VirtualJoystick _lc;
    VirtualJoystick _rc;

    public InputMobile(VirtualJoystick lc, VirtualJoystick rc)
    {
        _lc = lc;
        _rc = rc;
    }
    public Vector3 getAxis()
    {
        return new Vector3(_lc.Horizontal(),_lc.Vertical());
    }

    public Vector3 getFireAxis()
    {
        return new Vector3(_rc.Horizontal(), _rc.Vertical());
    }

    public bool getFire()
    {
        return _rc.IsDragging();
    }

    public void Update(){}

    public bool isPressing()
    {
        if (Input.touchCount > 0)
            return true;
        else
            return false;
    }
}
