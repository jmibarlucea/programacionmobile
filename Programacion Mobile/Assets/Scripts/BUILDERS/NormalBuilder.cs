﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalBuilder : IBuilder
{
    public void Build(GameObject zombie)
    {
        SpriteRenderer sr = zombie.GetComponent<SpriteRenderer>();
        CircleCollider2D cc = zombie.GetComponent<CircleCollider2D>();
        Player z = zombie.AddComponent<Player>();
        sr.sprite = Resources.Load<Sprite>("Zombie");
        cc.isTrigger = true;
        zombie.name = "Normal";
        zombie.tag = "Enemy";
        z.hp = 5;
        z.sp = 1;
        z.dmg = 1;
    }
}
