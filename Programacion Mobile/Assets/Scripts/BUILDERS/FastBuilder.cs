﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastBuilder : IBuilder
{
    public void Build(GameObject zombie)
    {
        SpriteRenderer sr = zombie.GetComponent<SpriteRenderer>();
        CircleCollider2D cc = zombie.GetComponent<CircleCollider2D>();
        Player z = zombie.AddComponent<Player>();
        sr.sprite = Resources.Load<Sprite>("Zombie");
        zombie.tag = "Enemy";
        zombie.name = "Fast";
        cc.radius = 0.2f;
        z.sp = 3;
        z.hp = 1;
        z.dmg = 2;
        sr.color = Color.red;
    }
}
