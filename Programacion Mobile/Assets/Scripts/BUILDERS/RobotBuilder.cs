﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotBuilder : IBuilder
{
    public void Build(GameObject robot)
    {
        SpriteRenderer sr = robot.GetComponent<SpriteRenderer>();
        CircleCollider2D cc = robot.GetComponent<CircleCollider2D>();
        Robots r = robot.AddComponent<Robots>();
        sr.sprite = Resources.Load<Sprite>("Robot");
        cc.isTrigger = true;
        cc.radius = 0.2f;
        robot.name = "Robot";
        r.sp = 2f;
        r.hp = 3;
        r.dmg = 2;
    }

}
