﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBuilder{

    /// <summary>
    /// Builds the zombie.
    /// </summary>
    /// <param name="zombie">Zombie to build.</param>
    void Build(GameObject zombie);

}
