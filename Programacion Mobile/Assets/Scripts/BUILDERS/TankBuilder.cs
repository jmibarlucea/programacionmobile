﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankBuilder : IBuilder
{
    public void Build(GameObject zombie)
    {
        SpriteRenderer sr = zombie.GetComponent<SpriteRenderer>();
        CircleCollider2D cc = zombie.GetComponent<CircleCollider2D>();
        Player z = zombie.AddComponent<Player>();
        sr.sprite = Resources.Load<Sprite>("Zombie");
        cc.isTrigger = true;
        cc.radius = 0.2f;
        zombie.transform.localScale = new Vector3(2, 2, 2);
        zombie.tag = "Enemy";
        zombie.name = "Tank";
        z.sp = 0.5f;
        z.hp = 10;
		z.dmg = 5;
        sr.color = Color.blue;
    }
}
