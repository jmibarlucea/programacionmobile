﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerBehav : MonoBehaviour {

    private float angle;
	public static playerBehav instance;
    Rigidbody2D rg;
    public float speed = 150;
    private float shootCooldown = 0f;
    public float shootSpeed = 0;

	void Awake()
	{
		if (instance == null)
			instance = this;
	}
	void Start () {
        entityManager.instance.player = this.gameObject;
        rg = GetComponent<Rigidbody2D>();
	}
	
    void FixedUpdate()
    {
        movement();
		checkCorners ();
        shoot();
    }

    void movement()
    {
        rg.velocity = InputManager.instance.GetAxis() * speed * Time.deltaTime;
        rotation();
    }
    
    void rotation()
    {
#if UNITY_ANDROID
        if (InputManager.instance.GetFireAxis().magnitude >= 0)
        {
            angle = Mathf.Atan2(InputManager.instance.GetFireAxis().y, InputManager.instance.GetFireAxis().x);
            angle *= Mathf.Rad2Deg;
        }
        
#elif UNITY_STANDAOLONE_WIN || UNITY_EDITOR

        angle = Mathf.Atan2(InputManager.instance.GetAxis().y, InputManager.instance.GetAxis().x);
        angle *= Mathf.Rad2Deg;
        Vector2 mp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        float AngleRad = Mathf.Atan2(mp.y - transform.position.y, mp.x - transform.position.x);
        angle = (180 / Mathf.PI) * AngleRad;
#endif
        rg.rotation = angle;
    }

    void shoot()
    {
        shootCooldown += Time.deltaTime;
        if (InputManager.instance.GetFire() && canShoot())
        {
            BulletPool.instance.Fire();
            shootCooldown = 0;
        }
    }

	public Vector3 getPos()
	{
		return transform.position;
	}

	void checkCorners()
	{ 
		
		if (transform.position.x <= -4.56f) {
			transform.position = new Vector2(-4.56f,transform.position.y);
		}
		if (transform.position.x >= 4.95f) {
			transform.position = new Vector2(4.95f,transform.position.y);
		}

		if (transform.position.y <= -5.13f) {
			transform.position = new Vector2(transform.position.x,-5.13f);
		}
		if (transform.position.y >= 4.35) {
			transform.position = new Vector2(transform.position.x,4.35f);
		}

	}

    private bool canShoot()
    {
        if (shootCooldown >= shootSpeed)
            return true;
        return false;
    }

}
