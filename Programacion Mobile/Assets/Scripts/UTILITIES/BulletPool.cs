﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPool : MonoBehaviour {

    public float cooldown = 0.5f; 
    public GameObject bullet;
    public int pooledAmount = 20;
    private List<GameObject> bullets;
    public static BulletPool instance;
    public GameObject Gun;

    void Awake()
    {
        if(instance == null)
            instance = this;
    }


    void Start()
    {
        bullets = new List<GameObject>();

        for (int i = 0; i < pooledAmount; i++)
        {
            GameObject obj = Instantiate(bullet);
            obj.SetActive(false);
            bullets.Add(obj);
        }
    }

    void Update()
    {
    }

    /// <summary>
    /// Spawns the bullet
    /// </summary>
    public void Fire()
    {
        for (int i = 0; i < bullets.Count; i++)
        {
            if (!bullets[i].activeInHierarchy)
            {
                bullets[i].transform.position = Gun.transform.position;
                bullets[i].transform.rotation = Gun.transform.rotation;
                bullets[i].tag = "PlayerBullet";
                bullets[i].SetActive(true);
                break;
            }
        }
    }

    public void EnemyFire(GameObject enemy)
    {
        for (int i = 0; i < bullets.Count; i++)
        {
            if (!bullets[i].activeInHierarchy)
            {
                bullets[i].transform.position = enemy.transform.position;
                bullets[i].transform.rotation = enemy.transform.rotation;
                bullets[i].tag = "EnemyBullet";
                bullets[i].SetActive(true);
                break;
            }
        }
    }
}
