﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class timeCount : MonoBehaviour {

	Text text;
	private float timecont;


	void Start()
	{
		timecont = 0;
		text = GetComponent<Text> ();
	}

	void Update()
	{
		timecont += Time.deltaTime;
		text.text = timecont.ToString("F0");
	}
}
