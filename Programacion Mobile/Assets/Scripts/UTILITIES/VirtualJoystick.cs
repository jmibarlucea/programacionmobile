﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using System;

public class VirtualJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler,IPointerDownHandler {

    private Image bgImg;
    private Image joystickImg;
    private Vector3 inputVector;
    private bool isDragging;


    private void Start()
    {
        bgImg = GetComponent<Image>();
        joystickImg = transform.GetChild(0).GetComponent<Image>();
    }

    public virtual void OnPointerDown(PointerEventData ped)
    {
        isDragging = true;
        OnDrag(ped);
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        inputVector = Vector3.zero;
        joystickImg.rectTransform.anchoredPosition = Vector3.zero;
        isDragging = false;
    }

    public virtual void OnDrag(PointerEventData ped)
    {
        Vector2 pos;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(bgImg.rectTransform, ped.position,ped.pressEventCamera,out pos))
        {
            pos.x = (pos.x / bgImg.rectTransform.sizeDelta.x);
            pos.y = (pos.y / bgImg.rectTransform.sizeDelta.y);

            inputVector = new Vector3(pos.x, pos.y);
            inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;

            //Mover la imagen del joystick

            joystickImg.rectTransform.anchoredPosition = new Vector3(inputVector.x  * (bgImg.rectTransform.sizeDelta.x/2f)
                                                                    , inputVector.y * (bgImg.rectTransform.sizeDelta.y/2f));

        }
    }

    public float Horizontal()
    {
        if(inputVector.x != 0)
        {
            return inputVector.x;
        }
        return Input.GetAxis("Horizontal");
    }

    public float Vertical()
    {
        if (inputVector.x != 0)
        {
            return inputVector.y;
        }
        return Input.GetAxis("Vertical");
    }
    
    public bool IsDragging()
    {
        return isDragging;
    }

}
