﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour {

    public GameObject z;
    public int pooledAmount = 100;
    public static EnemySpawn instance;
    public float spawnRate;
    private float spawnTime = 0;
    private List<GameObject> enemies;
    enemyType et;

     void Awake()
    {
        if (instance == null)
            instance = this;
    }

     void Start()
    {
        enemies = new List<GameObject>();
		for (int i = 0; i < pooledAmount; i++)
		{
			GameObject obj = Instantiate(z);
			BuildEnemy(obj);
			enemies.Add(obj);
			obj.SetActive(false);
		}
    }

    void Update () {
        Spawn();
        spawnTime += Time.deltaTime;
	}

    #region Methods
    //Checks if it's time to pool
    private bool canSpawn()
    {
        if (spawnTime >= spawnRate)
            return true;
        return false;
    }

    /// <summary>
    /// Build the zombie.
    /// </summary>
    private void BuildEnemy(GameObject enemy)
    {
        int rand = Random.Range(0,4);

        switch (rand)
        {
            case 0:
                et = enemyType.Normal;
                break;
            case 1:
                et = enemyType.Fast;
                break;
            case 2:
                et = enemyType.Tank;
                break;
            case 3:
                et = enemyType.Robot;
                break;
        }
        factoryManager.instance.enemyFactory.Create (enemy,et);
    }



    /// <summary>
    /// Spawn a zombie from the pool in a random place in your range.
    /// </summary>
    private void Spawn()
    {
        if (!canSpawn())
            return;

        // Create a random point in a range, hardcoded to be 5.

        Vector2 tmp = Random.insideUnitCircle;
        tmp.Normalize();
        tmp *= 5;

        //Pools a zombie in a random range from your player

        int rand = Random.Range(0,enemies.Count);
        if (enemies[rand].activeInHierarchy)
            Spawn();

        enemies[rand].transform.position = new Vector3(tmp.x + playerBehav.instance.getPos().x, tmp.y + playerBehav.instance.getPos().y, 0);
        enemies[rand].SetActive(true);
        spawnTime = 0;

    }
    #endregion
}
