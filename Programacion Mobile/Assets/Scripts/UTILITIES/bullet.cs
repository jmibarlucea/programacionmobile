﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour {

    Rigidbody2D body;   
    public float speed;
    int enemyDmg = 2;
    float cont = 0;
    SpriteRenderer sr;

    void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        body = GetComponent<Rigidbody2D>();
    }

    void OnEnable()
    {
        body.AddForce(transform.right * speed);
    }

    private void Update()
    {
        if (tag == "PlayerBullet")
            sr.color = Color.blue;
        if (tag == "EnemyBullet")
            sr.color = Color.red;
        cont += Time.deltaTime;

        if(cont > 2)
        {
            this.gameObject.SetActive(false);
            cont = 0;
        }
        
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Enemy" && this.tag == "PlayerBullet")
        {
            collision.GetComponent<Enemies>().getHit(1);
            this.gameObject.SetActive(false);
        }

        if (collision.tag == "Player" && this.tag == "EnemyBullet")
        {
            LifeManager.instance.getHit(enemyDmg);
            this.gameObject.SetActive(false);
        }
        
    }
}
