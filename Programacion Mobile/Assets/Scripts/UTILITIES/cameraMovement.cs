﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class cameraMovement : MonoBehaviour {

	public GameObject player;
	private Vector3 offset;

	void Start () 
	{
		offset = transform.position - player.transform.position;
	}
		
	void Update () 
	{
		transform.position = player.transform.position + offset;
        checkCorners();
    }

    /// <summary>
    /// Checks if camera is within boundaries
    /// </summary>
    void checkCorners()
    {
        if (transform.position.x <= -1.53f)
        {
            transform.position = new Vector3(-1.53f, transform.position.y, transform.position.z);
        }
        if (transform.position.x >= 1.86f)
        {
            transform.position = new Vector3(1.86f, transform.position.y, transform.position.z);
        }

        if (transform.position.y >= 2.69f)
        {
            transform.position = new Vector3(transform.position.x, 2.69f, transform.position.z);
        }
        if (transform.position.y <= -3.48f)
        {
            transform.position = new Vector3(transform.position.x, -3.48f, transform.position.z);
        }
    }
}
