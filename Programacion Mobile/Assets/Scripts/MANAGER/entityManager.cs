﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class entityManager : MonoBehaviour {

    public GameObject player;
    public List<GameObject> enemies;

    public static entityManager instance;
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }
}