﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class factoryManager : MonoBehaviour {

    public static factoryManager instance;
    public IFactory enemyFactory;


    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    private void Start()
    {
        enemyFactory = new EnemyFactory();
    }
}
