﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    VirtualJoystick leftController;
    VirtualJoystick rightController;
    IInput input;
    public static InputManager instance;
    public GameObject leftStick;
    public GameObject rightStick;

    void Awake()
    {
        if (!instance)
            instance = this;
        else
            Destroy(this.gameObject);

        #if UNITY_ANDROID

        leftStick.SetActive(true);
        rightStick.SetActive(true);
        leftController = leftStick.GetComponent<VirtualJoystick>();
        rightController = rightStick.GetComponent<VirtualJoystick>();
        input = new InputMobile(leftController,rightController); 

        #elif UNITY_STANDAOLONE_WIN || UNITY_EDITOR
        input = new InputKeyboard();
        #endif
    }

    void Update()
    {
        input.Update();
    }

    public Vector3 GetAxis()
    {
        return input.getAxis();
    }
    
    public bool GetFire()
    {
        return input.getFire();
    }


    public bool isPressing()
    {
        return input.isPressing();
    }

	public IInput getInput()
	{
		return input;
	}

    public Vector3 GetFireAxis()
    {
        return input.getFireAxis();
    }
}