﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeManager : MonoBehaviour {

	public static LifeManager instance;
	public int health;
    private int totalHealth;
	public GameObject Player;
    public Image lifeBar;

	void Awake()
	{
		if (instance == null)
			instance = this;
	}

    private void Start()
    {
        totalHealth = health;
    }

	void Update()
	{
		if (health <= 0) {
            Player.SetActive(false);
            Time.timeScale = 0;
        }
        lifeBar.fillAmount = (float)health / (float)totalHealth;
	}

	public void getHit(int dmg)
	{
		health -= dmg;
	}

    public void restoreHealth(int restoreHealth)
    {
        health += restoreHealth;
        if (health > totalHealth)
            health = totalHealth;
    }
}
