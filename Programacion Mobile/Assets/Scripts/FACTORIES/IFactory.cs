﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFactory{

    void Update();
    void Create(GameObject enemy, enemyType enemyType);
}
