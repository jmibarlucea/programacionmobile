﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFactory: IFactory{

    IBuilder builder;

    public void Create(GameObject enemy, enemyType enemyType)
    {
        switch (enemyType)
        {
            case enemyType.Normal:
                builder = new NormalBuilder();
                break;
            case enemyType.Fast:
                builder = new FastBuilder();
                break;
            case enemyType.Tank:
                builder = new TankBuilder();
                break;
            case enemyType.Robot:
                builder = new RobotBuilder();
                break;
        }
		builder.Build(enemy);
    }
    public void Update() { }
}
