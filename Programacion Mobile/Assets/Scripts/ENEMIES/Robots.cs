﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BehavType
{
    Following, Attacking
}

public class Robots : Enemies {
    float fireCadence = 0;
    float detectDistance = 1.5f;
    float loseDistance = 3;
    float distance;
    public BehavType behav;
    public override void executeBehav()
    {
        switch (behav)
        {   
            case BehavType.Following:
                followPlayer();
                break;
            case BehavType.Attacking:
                Attack();
                break;
            default:
                break;
        }

    }

    public override void followPlayer()
    {
        distance = Vector3.Distance(entityManager.instance.player.transform.position, this.transform.position);
        transform.position += transform.right * speed * Time.deltaTime;
        if (distance <= detectDistance)
        {
            behav = BehavType.Attacking;
        }
    }

    public void Attack()
    {
        fireCadence += Time.deltaTime;
        distance = Vector3.Distance(entityManager.instance.player.transform.position, this.transform.position);
        if (distance >= loseDistance)
            behav = BehavType.Following;
       
        if (fireCadence < 3)
            return;

        Fire();
        
    }

    public void Fire()
    {
        BulletPool.instance.EnemyFire(this.gameObject);
        fireCadence = 0;
    }

}
