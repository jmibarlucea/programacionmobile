﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : Enemies {


    public override void executeBehav()
    {
        followPlayer();
        checkHealth();
    }
    public override void followPlayer()
	{
		transform.right = playerBehav.instance.getPos () - transform.position;
		transform.position += transform.right * speed * Time.deltaTime;
    }

    public override void getHit(int damage)
    {
        base.getHit(damage);
        transform.position -= transform.right * 0.05f;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
			LifeManager.instance.getHit(damage);
			Destroy(this.gameObject);
        }
    }
}
