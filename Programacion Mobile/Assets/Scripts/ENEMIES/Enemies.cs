﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum enemyType
{
    Normal, Fast, Tank, Robot
}

public abstract class Enemies : MonoBehaviour {

    protected Vector3 dir;
    protected float speed;
    protected int health;
    protected int damage;
    protected SpriteRenderer sr;

    public float sp { get { return speed; } set { speed = value; } }

    public int hp { get { return health; } set { health = value; } }

    public int dmg { get { return damage; } set { damage = value; } }

    public void Awake()
    {
       
    }
    public void Start()
    {
        this.tag = "Enemy";
        sr = GetComponent<SpriteRenderer>();
        entityManager.instance.enemies.Add(this.gameObject);
    }
    public virtual void Update()
    {
        seePlayer();
        executeBehav();
        checkHealth();
    }

    #region Behaviours

    public virtual void followPlayer() {}

    public virtual void executeBehav() {}

    public virtual void checkHealth()
    {
        if (health <= 0) {
            this.gameObject.SetActive(false);
        }
    }

    public virtual void getHit(int damage)
    {
        hp -= damage;
    }

    public virtual void seePlayer()
    {
        transform.right = playerBehav.instance.getPos() - transform.position;
    }

    #endregion
}
