﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PowerUps : MonoBehaviour {

    CircleCollider2D cc;
    private void Start()
    {
        cc = GetComponent<CircleCollider2D>();
    }

    public virtual void OnTriggerEnter2D()
    {
        //DO SOMETHING     
    }
}
