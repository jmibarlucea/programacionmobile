﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthUp : PowerUps {

    int restoreHealth;

    public override void OnTriggerEnter2D()
    {
        base.OnTriggerEnter2D();
        LifeManager.instance.restoreHealth(restoreHealth);
    }
}
