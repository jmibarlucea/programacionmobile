﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

    public static LevelManager instance;
    private float LevelTime = 0;
    private float LevelUpTime = 10;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    private void Update()
    {
        LevelTime += Time.deltaTime;
        if (LevelTime >= LevelUpTime)
            LevelUp();
        Debug.Log(EnemySpawn.instance.spawnRate.ToString());
    }

    private void LevelUp()
    {
        LevelUpTime += 10;
        EnemySpawn.instance.spawnRate -= 0.5f;
    }
}
